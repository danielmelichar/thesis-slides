<style type="text/css">
  .left {
    text-align: left !important;
    display: block !important;
</style>

<!------------------------------  INTRODUCTION  ------------------------------>
# Safety Analysis Of
# Deep Reinforcement
# Learning Agents

Zwischenstand IV

Daniel Melichar

<small>
Mat. Nr. 01650759
</br>
Pers. Kez. 1820258003
</small>

--

**Understand a system and its attributes, design a rule-based solution** <!-- .element: class="fragment" data-fragment-index="1" -->

**Let a machine modify itself until it formulates rules that fit the solution** <!-- .element: class="fragment" data-fragment-index="2" -->

Note:
How to compute

* Manually define statements and conditions -> good cause the system is interpretable
* Let a program approximate the most likely rule -> can handle larger number of state spaces

--

![Auton. Syste](./res/autovehic.png)

<small>Source: Kiran, B. R. et al.</small>

Note:
use automotive industry. manual rule definition is practically infeasble.

--

### How to ensure an automated decision making system does not pick choices that are harmful?



<!------------------------------ THEORY ------------------------------>
# Deep
# Reinforcement
# Learning

--

**Goal of RL**

`$\theta^* = arg \underset{\theta}{max} E_{\tau \sim p_\theta(\tau)}[\sum_{t}^{T} r(s_t, a_t)]$`

![Deep RL](./res/deeprl.png) <!-- .element: height="50%" width="50%" -->

<small>Source: Sergey Levine, CS285</small>

--

<section style="text-align: left; margin: auto; vertical-align:middle;">

Idea 1: If `$Q^\pi(s, a)$` of policy `$\pi_A$` is equal, or bigger, than `$Q^\pi(s, a)$` of policy `$\pi_B$`, we assume that `$\pi_A$` has improved.

1. Compute `$Q(s, a)$`
2. Explore or exploit (e.g. epsilon-greedy, UCB)
3. Set `$\pi(s) = argmax_aQ(s,a)$`

**Known as Value Optimization** <!-- .element: class="fragment" data-fragment-index="1" -->

---

Idea 2: Compute gradient to update parameterized policy `$\pi_\theta$` with maximized return as performance function

1. Define performance, often `$A^\pi(s_t, a_t) =  Q^\pi(s_t, a_t) - V^\pi(s_t)$`
2. Explore or exploit (e.g. epsilon-greedy, UCB)
3. Update parameters

**Known as Policy Gradient Optimization** <!-- .element: class="fragment" data-fragment-index="2" -->
</section>

Notes:
+ Performance function could be `$V^\pi(s_t)$` or `$Q^\pi(s_t, a_t)$` or advantage function `$A^\pi(s_t, a_t) =  $Q^\pi(s_t, a_t) - V^\pi(s_t)$`$`
+ Imitation also exists




<!------------------------------ EXPERIMENTS ------------------------------>

# Experiments

--

## Setup <!-- .element: class="left" -->

<ul class="left">
<li> Environment: Discrete <bold>LunarLandar</bold></li>
<li> Agents</li>
    <ul class="left">
        <li> Value-based: <bold>DQN</bold></li>
        <li> Actor-Critic: <bold>A2C</bold></li>
    </ul>
<li> Goal: Land on moon</li>
<li> Safety function: Must remain within 20% treshold of landing coordinates</li>
</ul>

--

![DQN Control](./res/start.gif) <!-- .element: height="50%" width="50%" -->

--

![DQN Control](./res/middle.gif) <!-- .element: height="50%" width="50%" -->

--

<p class="stretch"><img src="./res/modifications.png"></p>

--

![DQN Control](./res/DQNControlAgent.gif) <!-- .element: height="50%" width="50%" -->



# Results

--

<p class="stretch"><img src="./res/scores.png"></p>

--

<p class="stretch"><img src="./res/safety.png"></p>

--

## Takeaways <!-- .element: class="left" -->

<ul class="left">
<li> Deep RL takes a lot of compute</li>
<li> DQN works well if the problem is discrete</li>
<li> A seperate controller will help with safety during training, but ...</li>
<li> ... that does not mean it will work or is even applicable in the real world</li>
<li> Just adding a reward doesn't do much</li>
</ul>



<!------------------------------ CONTRIBUTION ------------------------------>

## Contribution <!-- .element: class="left" -->

<ul class="left">
<li> Introduction to concepts and methods of Deep RL. </li>
<li> Conducted experiments. </li>
  <ul class="left">
    <li> Implemented of two state-of-the-art algorithms. </li>
    <li> Showed challenges in reward signal design. </li>
    <li> Showed improvements during learning with controller. </li>
  </ul>
</ul>



<!------------------------------ ADDITIONAL ------------------------------>
# Additional formalism

--

Sources

<ul class="left">
<li>Kiran, B. R. et al. “Deep Reinforcement Learning for Autonomous Driving: A Survey.” ArXiv abs/2002.00444 (2020): n. pag.</li>
<li>Hernández-Orallo, J. et al. “AI Paradigms and AI Safety: Mapping Artefacts and Techniques to Safety Issues.” ECAI (2020). </li>
</ul>

--

## Future work  <!-- .element: class="left" -->

**For value-based methods: use different exploration strategies** <!-- .element: class="left" -->

<ul class="left">
<li> Optimistic exploration </li>
<li> Posterior sampling </li>
<li> Information gain </li>
</ul>

**For policy-based methods: use different optimisation techniques** <!-- .element: class="left" -->

<ul class="left">
<li> Bayesian optimisation </li>
</ul>

**Other** <!-- .element: class="left" -->

<ul class="left">
<li> Automatic hyperparameter tuning </li>
<li> Human-in-the-loop </li>
<li> Neural net to learn reward function </li>
</ul>

Notes:
* This approach is also known as upper confidence bounds. The intuition behind is to basically try each action until its certain the action is not great. The algorithm measures the potential of actions to have an optimal value by giving it an upper confidence bound of the estimated reward value.
* This approach, also known as Thompson Sampling, essentially uses generated samples to implement the idea of probability matching. That is, after each time step, the estimates of the rewards are updated with an episode's outcome.
* With this approach we calculate the entropy loss term, and generally learn how much new information can be extracted from actions.

--

| Description                                 | Formula                                                                                                                                     | Definiton                                                                                      |
| ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| Total sum of discounted rewards             | `$ G_t = R_{t+1} + \gamma R_{t+2} + \dotsb = \sum_{t}^{T} \gamma^t R_{t+k+1} $` <!-- .element: class="fragment" data-fragment-index="1" --> | **Known as Return** <!-- .element: class="fragment" data-fragment-index="2" -->                |
| Total reward from `$s_t$`                   | `$ V^\pi(s) = E_\pi [G_t \| S_t = s] $` <!-- .element: class="fragment" data-fragment-index="3" -->                                         | **Known as State-Value function** <!-- .element: class="fragment" data-fragment-index="4" -->  |
| Total reward from taking `$a_t$` in `$s_t$` | `$Q^\pi(s, a) = E_\pi[G_t \| S_t = s, A_t = a] $` <!-- .element: class="fragment" data-fragment-index="5" -->                               | **Known as Action-Value function** <!-- .element: class="fragment" data-fragment-index="6" --> |

--

![MDP](./res/mdp.svg) <!-- .element: height="50%" width="50%" -->

--

### Partially observed markov decision process

`$ M = {S, A, O, T, \epsilon, r} $`

|                                     |                                                     |
| ----------------------------------- | --------------------------------------------------- |
| `$S$` - state space                 | states `$s \in S$` (discrete or continuous)         |
| `$A$` - action space                | actions `$a \in A$` (discrete or continuous)        |
| `$O$` - observation space           | observations `$o \in O$` (discrete or continuous)   |
| `$T$` - Transition operator         | `$ p (s_{t+1} \| s_t ) $`                           |
| `$\epsilon$` - Emission probability | `$ p (o_t \| s_t ) $`                               |
| `$r$` - Reward function             | `$r(s_t, a_t)$` which states and actions are better |

--

![Comparisson](https://github.com/boliu68/boliu68.github.io/blob/master/_posts/one_state_rl.png?raw=true)

--

### Machine Learning

| **Unsupervised** | **Supervised** | **Reinforcement** |
| ---------------- | -------------- | ----------------- |
| Unlabeled data   | Labeled data   | Unlabeled data    |
| Find structure   | Predict        | Predict           |

--

### DQN

![DQN](./res/dqn.png)

--

### A2C

![A2C](./res/a2c.png)

--

### Exploration strategies

![Exploration](./res/exploration.png) <!-- .element: height="50%" width="50%" -->

--

### Gaussian process

![Gaussian](./res/gaussian.png)

<small>Source: arxiv.org/pdf/1602.04450v1.pdf</small>

--

### Unfold MDP

|   |
|---|
|![mdp1](./res/unfold-mdp1.png) |
|![mdp2](./res/unfold-mdp2.png) |

<small>Source: yisongyue.com/publications/aaai2018_safe_mdp.pdf</small>

--

![People](./res/people.png)

--

![Gaussian](./res/gaussian.gif)
