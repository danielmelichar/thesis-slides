<!------------------------------ START PART 1 ------------------------------>
# Bachelor Arbeit
Zwischenstand von Daniel Melichar

--

## Central Questions
- What are current algorithms for Deep RL? How do they differ? What improvements are made? <!-- .element: class="fragment" data-fragment-index="1" -->
- How safe are autonomous agents? Which methods can be applied? <!-- .element: class="fragment" data-fragment-index="2" -->
- What are real world applications and challenges? <!-- .element: class="fragment" data-fragment-index="3" -->

--

## Content Areas
- **Deep RL:** Formalism, Algorithms, Challenges
- **Safety**: Standards, Black-Box and White-Box methods
- **Applications**: Driving, Flying

--

## Done so far
- Tried a lot of frameworks
- Experimented with algorithms
- Theoretical foundations of Deep RL
- What does _safety_ actually mean?
- Applied first safety measurements

--

## Lessons learned
- It's hard
- Code ASAP
- Ecosystem is unstable
- Reproducibility is not given
- Design experiments early
- Needs really good hardware

--

## Open tasks
- Apply black-box methods from Automatous Vehicles
- Implement different exploration strategies
- More formally define the concept
- Probably more




<!------------------------------ START PART 2 ------------------------------>
# Deep Reinforcement Learning

--

![RL Illustration](https://lilianweng.github.io/lil-log/assets/images/RL_illustration.png)

<small>Source: Lilian Weng</small>

Note:
+ An agent interacts with the environment, trying to take smart actions to maximize cumulative rewards (== Return)
+ State is a complete description of the world
+ Observation is a partial description of state

--

**Python Code**

``` python [5|9-22|30|34-45|50-59]
import random
import gym
from gym import spaces

class Environment(gym.Env):
    def __init__(self):
        self.steps_left = 10

    def get_observation(self):
      return [random.randint(0, 1) for _ in range(3)]

    def get_actions(self):
      return [0, 1]

    def is_done(self):
      return self.steps_left == 0

    def step(self, action):
      if self.is_done():
        raise Exception("Game is over")
      self.steps_left -= action
      return random.random()

    def render(self):
      pass

    def close(self):
      pass

class Agent:
    def __init__(self):
        self.total_reward = 0.0

    def step(self, env):
        current_obs = env.get_observation()
        all_actions = env.get_actions()

        if sum(current_obs) == 3:
          action = all_actions[1]
        else:
          action = random.choice(all_actions)

        reward = env.step(action)
        print(f"Agent: Action {action} : Reward {reward}")
        self.total_reward += reward

env = Environment()
agent = Agent()

while not env.is_done():
    agent.step(env)
    # Agent: Action 1 : Reward 0.7932088585443122
    # Agent: Action 0 : Reward 0.42970848353079627
    # Agent: Action 0 : Reward 0.6970854293445339
    # ...
    print("Total reward got: %.4f" % agent.total_reward)
    # Total reward got: 6.8300

```

--

## Additional Terminology

--

**Action Space**

<small>What can be done</small>

Discrete or Continuous

--

Deterministic Policy `$$ a_t = \mu_\theta(s_t) $$`

Stochastic Policy `$$ a_t \sim \pi_\theta(\cdot | s_t) $$`

<small>How to act</small>

Note:
+ Parameters solved by neural nets

--

**Trajectories**

<small>Sequence of states and actions</small>

`$$ \tau = (s_0, a_0, s_1, a_1, \ldots) $$`

Note:
+ Frequently known as episode

--

**Value function**

Expected return of policy

--

**Model-based, Model-free**

Is there a description of the environment?

Note:
+ hard for a lot of problems
+ easy for things like: chess, go, etc.
+ DeepMind and DOTA

--

**On-policy, off-policy**

Will explain a bit later.

--

**Markov Decision Process**

+ Formally almost all RL problems are MDPs
+ All states have a Markov property

`$$ M = <S, A, P, R, \gamma> $$`

- S - a set of states;
- A - a set of actions
- P - transition probability function;
- R - reward function
- $\gamma$ discounting factor for future rewards

Note:
Markov property: future only depends on current state, not history.

--

![Markov decision process of a typical workday](https://lilianweng.github.io/lil-log/assets/images/mdp_example.jpg)

<small>Source: randomant.net/reinforcement-learning-concepts</small>

--

## The RL Problem

`$$ \pi^* =  arg max_\pi E_{\tau\sim\pi} [R(\tau)] $$`

<small>Reinforcement Learning is about learning from interaction how to behave to achieve a goal</small>

Note:
+ The goal in RL is to select a policy which maximizes expected return when the agent acts according to it.



<!------------------------------ START PART 3 ------------------------------>
# Algorithms

--

+  Dynamic Programming (short)
+  Monte-Carlo Methods (short)
+  Temporal-Difference Learning
+  Policy Gradient

Note:
+ A lot of things will overlap

--

![Algorithms](./res/algorithms.png)

<small>Source: Intel AI Labs</small>

--

![Algorithms](./res/algorithms_value.png)

<small>Source: Intel AI Labs</small>

--

## Dynamic Programming

Solve a known MDP

+ Bootstrapping: Creating estimates out of estimates
+ Policy Evaluation: Compute the state-value Vπ for a given policy π:
+ Policy Improvement: generates a better policy π′≥ π
+ Policy Iteration

![policyiteration](./res/policyiteration.png)


--

## Monte Carlo Methods

Estimate the value function of an unknown MDP

+ No bootstrapping, learn from complete episodes
+ Exploration is an issue
+ Importance sampling

![MC](https://lilianweng.github.io/lil-log/assets/images/MC_control.png)

--

## Temporal Difference Learning

Optimise the value function of an unknown MDP

+ Learn from episodes of experience
+ Learn from incomplete episodes by bootstraping
+ Model free
+ Combining Dynamic Programming and Monte Carlo Methods

--

**Let's compare MC with TD**

--

![Monte Carlo Estimation](./res/mc-estimation.png)

<small>Greydanus & Olah, Distill, 2019</small>

--

![Monte Carlo Estimation](./res/td-estimation.png)

<small>Greydanus & Olah, Distill, 2019</small>

Note:
+ agent’s experience as a series of trajectories.
+ de-emphasizes relationships between trajectories
+ Monte Carlo is averaging over real trajectories whereas TD learning is averaging over all possible paths.

--

**Introducing Q-functions**

Instead of estimating the value of a state, estimate the value of a state and an action.

![Q-Functions](./res/qfunc.png)

<small>Greydanus & Olah, Distill, 2019</small>

--

Basically: Parameterized Temporal Difference

--

**On-policy**

![On-Policy](./res/on-policy.png)

<small>Levine, et. al. 2020</small>

Note:
+ Behaviour policy == Policy used for action selection
+ Examples: SARSA; PPO

--

**Off-policy**

![On-Policy](./res/off-policy.png)

<small>Levine, et. al. 2020</small>

Note:
+ Behaviour policy ≠ Policy used for action selection
+ Examples: Q learning, DQN

--

![Algorithms](./res/algorithms_value.png)

<small>Source: Intel AI Labs</small>

--

![Algorithms](./res/algorithms_policy.png)

<small>Source: Intel AI Labs</small>

--

## Policy Gradient

--

## Problem

So far the value functions are represented in a lookup table

+ Every state has an entry V(s)
+ Or every state-action pair s, a has an entry Q(s, a)
+ Does not scale very well
  + Backgammon: 10^20 states
  + Go: 10^170 states
  + Helicopter: continuous state space

--

## Solution

+ Use function approximators to obtain optimal rewards
+ They are more useful in continuous space.


Note:
Calculating value in infinite number of actions/states is computationally infeasible

--

![Policy Gradient](./res/policy_gradient.png)

<small>Schulmann et al., 2016</small>

--

![Policy Gradient](./res/policy_gradient_informal.png)

<small>Source: CS285 UC Berkley</small>

--

## Actor Critic

+ Separate learning what is value-able `V(s)` from learning what to do `π(s)`
+ Intuition: **combine Q-Learning with Policy Gradients**
+ **Critic** updates value function parameters
+ **Actor** updates the policy parametes

![actorcritic](./res/actorcritic1.png)

Note:


--

## Why so many algorithms?

- Different tradeoffs: Sample efficiency, stability
- Different assumptions
  - Stochastic or deterministic?
  - Continuous or discrete?
  - Episodic or infinite horizon
- Different representation methods: Represent in policy or model

--

## Quality metrics

**Sample complexity**

How many episodes do I need to obtain a good policy?

How easy is it to get a good policy without considering exploration issues?

**Regret**

How good is the exploration strategy used?

--

## Excelent reads

[Deep Reinforcement Learning Doesn't Work Yet](https://www.alexirpan.com/2018/02/14/rl-hard.html)

and

[Is Reinforcement Learning a fantasy?](http://www.richardloosemore.com/2016/10/25/is-reinforcement-learning-rl-a-fantasy/)



<!------------------------------ START PART 4 ------------------------------>
# Safety Issues

--

Let's first talk about the challenges in Deep RL independently of the domain.

--

## Known problems and algorithm challenges

- Stability and hyperparameter tuning
- Sample complexity
- Scaling & Generalization
- Reward Engineering / Problem Formulation
- **Exploration vs Exploitation**
  - The Hard Problem
  - The Noisy-TV problem
- **Sutton's Deadly Triad issue**

Note:
+ Hyperparameters cannot be changed in real world. How representive is simulator?
+ Lots and lots of examples needed, simulator needs to be amazing
+ It takes 6 days of real time to run on an infinite flat plane

--

## The Hard Problem

Sparse reward leads to big regret.

![Hard problem](./res/hardproblem.png)

<small>Source: CS285 UC Berkley</small>


Note:
+ Getting key = reward
+ Opening door = reward
+ Getting killed by skull = nothing (is it good? bad?)
+ Finishing the game only weakly correlates with rewarding events
+ We know what to do because we understandwhat these sprites mean!

--

## Definition

> How can an agent decide whether to attempt new behaviors (to discover ones with higher reward) or continue to do the best thing it knows so far?

**Exploitation**: doing what you know will yield the highest reward

**Exploration**: doing things you haven't done before, in hopes of getting even higher reward

--

![Regret](./res/regret.png)

<small>Source: CS285 UC Berkley</small>

This is especially hard in large continuous spaces.

It is why the context of the agent is often limited to be finite (e.g. with a model-based RL agent)

--

## The Noisy TV Problem

![Noisy TV](https://lilianweng.github.io/lil-log/assets/images/the-noisy-TV-problem.gif)

<small>Source: OpenAI Blog: “Reinforcement Learning with Prediction-Based Rewards“</small>

Note:
+ RL Agent is seeking novel experience
+ TV with uncontrollable and unpredictable random noise ouputs stuff
+ The agent obtains new rewards from noisy TV consistently
+ Couch potato

--

## Sutton's Deadly Tirad Issue

The value function may suffer from instabilities and/or divergence.

![Q-Learning Equation](./res/deadlytriad1.png)

<small>Source: https://k4ntz.medium.com/</small>

Function Approx. might

`$$ Q(S, a) \approx Q(S', a_1) $$`

Note:
+ In the beginning: Q(S, a) ~= 3
+ Func Approx. might because S and S' are similar
+ This might converge to Q(S', a1) ~= 3

--

![Converge](./res/deadlytriad2.png)

<small>Source: https://k4ntz.medium.com/</small>

Note:
Although Reward is set, Q converges

--

**How?**

Bootstrapping: Creates estimate of Q-value, the return of a state/action pair

Off-policy learning: Best Q-Value of the next state

Function Approx: They are the ones approximating

--

**Example: Mario**

+ Going forward: +1
+ Running anywhere else: -1
+ Dying: -10

![Mario](https://miro.medium.com/max/700/1*cO3bY2qETIo1VgAmWENKpA.jpeg)

<small>Source: https://k4ntz.medium.com/</small>

Note:
+ States are similar. Converges happen. Mario keeps running into Goomba.
+ Replay Buffer
+ Periodically updated target (Actor/Critic)

--

## Three major classes

![Classes](./res/exploration_classes.png)

<small>Source: CS285 UC Berkley</small>

Most current algorithms are based on these ideas.

Note:
1. Keep track of average reward for each action. Try each action until you are **sure** its not great
2. Rather than simple average across all actions, build a probability model for each reward and samples from this to choose action
3. Reason about gain from visiting new states

--

## Classic Exploration Strategies

- Optimistic exploration
  - New states are good states
  - Count how often states are visited
  - Examples: Epsilon-Greedy or Upper Confidence bounds
- Thompson sampling style:
  - Learn distribution over Q-functions or policies
  - Sample and act according to sample
- Information gain style:
  - Reason about gain from visiting new states

--

## A few current strategies

<small>Source: Lilian Weng</small>

- Intrinsic Rewards as Exploration Bonuses
  - Count-based Exploration
    - Counting by Density Model
    - Counting after Hashing
  - Prediction-based Exploration
    - Forward Dynamics
    - Random Networks
    - Physical Properties
- Memory-based Exploration
  - Episodic Memory
  - Direct Exploration
- Q-Value Exploration
- Varitional Options


<!--
ToDo: https://lilianweng.github.io/lil-log/2020/06/07/exploration-strategies-in-deep-reinforcement-learning.html
-->

--

![Almost done!](https://media0.giphy.com/media/xT9IgIyroYfpQXtpaE/giphy.gif?cid=ecf05e478lb8j1i10zmi2zd24dau2aqc830404fze19t7jxs&rid=giphy.gif)

--

## So how is this possible?

![Collage](./res/collage.png)

--

![Levels of automation](https://www.synopsys.com/content/dam/synopsys/solutions/automotive/levels-of-driving-automation.jpg.imgw.850.x.jpg)

<small>Source: Synopsys</small>

--

## Human-In-The-Loop Learning

Also known as Active Learning Pipeline or Data Engine

![Active Learning](./res/aktivelearning.png)

<small>Source: Lex Fridman, MIT Deep Learning 2020</small>

--

## Multi-Task Learning

![Multi Task](./res/multi.png)

<small>Source: Lex Fridman, MIT Deep Learning 2020</small>

--

## Safety Domains

--

![AI Safety issue groups](./res/aisafetygroups.png)

<small>Hernández-Orallo et al, 2020</small>
<!--
AI Paradigms and AI Safety:Mapping Artefacts and Techniques to Safety Issues
-->

--

**SPECIFICATION**

Behave according to intentions.

**ROBUSTNESS**

Withstand rare cases adversaries.

**ASSURANCE**

Analyzing, monitoring, and controlling systems during operation.

--

## Robustness

Autonomous devices are entering safety-critical domains. They need to be tested thoroughly before they are deployed.

**Everything we talked about before is known as white-box methods**. We use the **knowledge we have of the system** to make it better. **Scalability** is a big problem, though (e.g. large neural nets).

Instead we should seek to use **black-box** algorithms for safety validation, to seek out failure cases and retrain our network with them.

--

![Black box validatio](https://ai.stanford.edu/blog/assets/img/posts/2020-08-25-black-box-safety-validation/formulation.jpg)

<small>Source: Anthony L. Corso, SAIL</small>

Note:
State = full, Observation = partial

--

## Generating Disturbances

<!--
https://ai.stanford.edu/blog/black-box-safety-validation/
-->

+ **Optimization** approaches search over the space of possible trajectories to find those that lead to system failure. For example: at which states were two vehicles the closest?
+ When the safety validation problem is cast as a **path-planning** problem, we search for failures by sequentially building disturbance trajectories that explore the state space of the environment. For example: generate complex acceleration and velocity profiles that would be rarely used.
+ **Multi-Agent RL** or **Advanced Actor Critic** can be used to monitor the "doing" agent from the "safety" agent.
+ **Importance sampling** seeks to learn a sampling distribution that reliably produces failures. Some common approaches are the cross-entropy method, supervised learning (with labels from the driver).

--

## Assurance

![assurance](./res/assurance.png)

<small>Olah et al., 2018</small>

--

## Specification

**Example: Collision Detection**

+ Agent: `(car.x, car.y)`
+ Obstacle: `(obs.x, obs.y)`
+ Saftey constraint `(car.x != obs.x AND car.y != obs.y)`

**Not sufficient!**  <!-- .element: class="fragment" data-fragment-index="1" -->

Note:
The car must instead correctly compute its braking distance based on a dynamical model of the car and the obstacle, so that it starts braking with sufficient lead time to ensure the car comes to a complete stop before reaching an obstacle.s

--

## Specification

![Formal Methods](./res/formalmethods.png)

<small>Hunt et al., 2020</small>

--

## International Standards

The ISO26262, "Road vehicles – Functional safety" is also helpful.

It's a **white-box** safety measurement.

![iso](https://media.springernature.com/original/springer-static/image/chp%3A10.1007%2F978-3-319-64218-5_44/MediaObjects/455179_1_En_44_Fig2_HTML.gif)

--

![Development Process](./res/process1.png)

<small>Source: NHTSA</small>

--

![Development Process](./res/process2.png)

<small>Source: Anthony L. Corso, SAIL</small>





<!------------------------------ START PART 5 ------------------------------>
# Experiments

--

## Concept

1. Start with to implement a simple Tabular Algorithm
2. Compare Tabular Methods to Policy Gradients
3. Look at advanced, currently researched algorithms mostly based on Actor-Critics
4. Open: Apply safety methods

--

![DQN](./res/dqn.png)

Note:
+ **Experience Replay** Instead of directly learning from one experience, do so from a pool randomly.
+ **Seperate Target Network**: We are trying to find the best Q' based on the current Q. But the states S' and S are very similar. Hence Deadly Triad. To avoid this we include a second network that gets updated only occasionally. This second network is never trained, the paramters are only copied. In Actor Critic, they are both trained

--

## DQN vs PPO


Off-Policy vs On-Policy(-ish)

Tabular Methods vs Policy Gradient

Extensions of DQN: Double, Dueling, PER

Note:
+ PPO on policy ish: does not deviate too much (proxiamal) but uses samples from slightly older policy
+ PER: Instead of randomly selecting from experience replay buffer, select the replays with the higest priority. Measures by magnitude of tempora-difference error

--

**Remember: Actor Critic**

![actorcritic](./res/actorcritic2.png)

<small>Source: https://medium.com/emergent-future</small>

--

## Actor Critics and Memory

+ **Deep Deterministic Policy Gradient** (DDPG): Model-free off-policy actor-critic algorithm, combining DQN with Deep Policy Gradients
+ **Soft Actor Critic** (SAC): Model-free off-policy actor-critic with maximum entropy to enable stability and exploration
+ **Twin Delyed DDPG** (TD3): Incoporate idea from Double DQN (decouble Q-value update and action selection) and smoothing into DDPG
+ **Hindsight Experience Replay** (HER): Method to learn from trajectories despite not having received the actual goal. It basically at least learns _something_ from a failed attempt.


Note:
+ **For HER:**
+ No matter how bad our policy is, it will always have some positive rewards to learn from
+ Intuition is like imagine wanting to make yourself a coffee. your goal is to have the perfect ratio of milk and sugar. you experiment with the exact amounts of sugar cubes and milk. one day you explore and add one cube of sugar too much. instead of seeing this as a complete failure, you say that you now know that you know what not to do

--

# What's next?

- Apply black-box methods from automatous vehicles
- Implement different exploration strategies
- Formalism of experiments, slides, notes

<!-- [[Paper](https://arxiv.org/pdf/1912.03618.pdf)](./res/paper.png) -->
