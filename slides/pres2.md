
<!------------------------------  INTRODUCTION  ------------------------------>
# Bachelor Arbeit
Zwischenstand III

Daniel Melichar

<small>
Mat. Nr. 01650759
</br>
Pers. Kez. 1820258003
</small>

--

**Understand a system and its attributes, design a rule-based solution** <!-- .element: class="fragment" data-fragment-index="1" -->

![Classical programming](./res/classical.svg) <!-- .element: class="fragment" data-fragment-index="1" height="50%" width="50%" -->

**Let a machine modify itself until it formulates rules that fit the solution** <!-- .element: class="fragment" data-fragment-index="2" -->

![Machine Learning](./res/ml.svg) <!-- .element: class="fragment" data-fragment-index="2" height="50%" width="50%" -->

--


<div class="r-stack">
    <img class="fragment" src="./res/problems1.png">
    <img class="fragment" src="./res/problems2.png">
</div>

<small>Source: doi.org/10.3233/FAIA200386</small>



<!------------------------------ THEORY ------------------------------>
# Deep
# Reinforcement
# Learning

--

**Goal of RL**

`$\theta^* = arg \underset{\theta}{max} E_{\tau \sim p_\theta(\tau)}[\sum_{t}^{T} r(s_t, a_t)]$`

![Deep RL](./res/deeprl.png) <!-- .element: height="50%" width="50%" -->

<small>Source: Sergey Levine, CS285</small>




<!------------------------------ EXPERIMENTS ------------------------------>

# Experiments

--

<div class="r-stack">
    <img class="fragment" src="./res/experiment1.svg" height="55%" width="55%">
    <img class="fragment" src="./res/experiment2.svg" height="55%" width="55%">
</div>

--

- **Option A:** Stochastic analysis over `$\tau$` given current `$\pi_\theta$` <!-- .element: class="fragment strike" data-fragment-index="1" -->
- **Option B:** Model adaptation with formal verification of distance between lander and bounds.

![LunarSafe-v0](./res/screenshot.png) <!-- .element: class="fragment" data-fragment-index="1" -->

Note:
Option B is often used in autom. vehicles

--

<section style="text-align: left; margin: 5%; vertical-align:middle;">

## Default

+ **DQN Agent** - Off-policy temporal difference value optimization.
+ **A2C Agent** - On policy temporal difference policy gradient optimization.


## Risk included in reward

+ **DQN Safe Agent** - Give penalty for unsafe states and reward safe states.
+ **A2C Safe Agent** - Same as above.

</br>

## Controller takeover

+ **DQN Control Agent** - If unsafe state is reached, use heuristics
+ **A2C Control Agent** - Same as above.

</section>

Note:
Credit assignment problem. Which actions have led to getting this reward?
In real world hard to do, no simulation

--

![Reward Mani.](./res/safe.svg) <!-- .element: height="50%" -->

--

![Controller](./res/controller.svg) <!-- .element: height="50%" -->

--

![DQN Control](./res/DQNControlAgent.gif) <!-- .element: height="50%" width="50%" -->



# Results

--

| **Policy optimization** |                                                     |
| --------------------------------------------------- | --------------------------------------------------- |
| ![Policy Scores](./res/plots/policy-scores.png) | ![Policy Safety](./res/plots/policy-safey.png) |
| **Value Optimization** |                                                     |
| --------------------------------------------------- | --------------------------------------------------- |
| ![Value Scores](./res/plots/value-scores.png) | ![Value Safety](./res/plots/value-safety.png) |

--

## Future: improve policy

Use different exploration strategies

Off policy evaluation with importance sampling

Direct policy improvement using gaussian process

Unfold all state spaces to finite markov decision process

Reward function with neural networks and human preferences


<!------------------------------ CONTRIBUTION ------------------------------>


**Argued that formal methods are a good method to maintain safety requirements during the training process, but may not be applicable in real world scenarios** <!-- .element: class="fragment" data-fragment-index="1" -->

**Showed the complexity of defining a reward signal is in relation to the agent's model of the world** <!-- .element: class="fragment" data-fragment-index="2" -->





<!------------------------------ ADDITIONAL ------------------------------>

# Additional formalism

--

| Description                                 | Formula                                                                                                                                     | Definiton                                                                                      |
| ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| Total sum of discounted rewards             | `$ G_t = R_{t+1} + \gamma R_{t+2} + \dotsb = \sum_{t}^{T} \gamma^t R_{t+k+1} $` <!-- .element: class="fragment" data-fragment-index="1" --> | **Known as Return** <!-- .element: class="fragment" data-fragment-index="2" -->                |
| Total reward from `$s_t$`                   | `$ V^\pi(s) = E_\pi [G_t \| S_t = s] $` <!-- .element: class="fragment" data-fragment-index="3" -->                                         | **Known as State-Value function** <!-- .element: class="fragment" data-fragment-index="4" -->  |
| Total reward from taking `$a_t$` in `$s_t$` | `$Q^\pi(s, a) = E_\pi[G_t \| S_t = s, A_t = a] $` <!-- .element: class="fragment" data-fragment-index="5" -->                               | **Known as Action-Value function** <!-- .element: class="fragment" data-fragment-index="6" --> |

--

<section style="text-align: left; margin: auto; vertical-align:middle;">

Idea 1: If `$Q^\pi(s, a)$` of policy `$\pi_A$` is equal, or bigger, than `$Q^\pi(s, a)$` of policy `$\pi_B$`, we assume that `$\pi_A$` has improved.

1. Compute `$Q(s, a)$`
2. Explore or exploit (e.g. epsilon-greedy, UCB)
3. Set `$\pi(s) = argmax_aQ(s,a)$`

**Known as Value Optimization** <!-- .element: class="fragment" data-fragment-index="1" -->

---

Idea 2: Compute gradient to update parameterized policy `$\pi_\theta$` with maximized return as performance function

1. Define performance, often `$A^\pi(s_t, a_t) =  Q^\pi(s_t, a_t) - V^\pi(s_t)$`
2. Explore or exploit (e.g. epsilon-greedy, UCB)
3. Update parameters

**Known as Policy Gradient Optimization** <!-- .element: class="fragment" data-fragment-index="2" -->
</section>

Notes:
+ Performance function could be `$V^\pi(s_t)$` or `$Q^\pi(s_t, a_t)$` or advantage function `$A^\pi(s_t, a_t) =  $Q^\pi(s_t, a_t) - V^\pi(s_t)$`$`
+ Imitation also exists

--

![MDP](./res/mdp.svg) <!-- .element: height="50%" width="50%" -->

--

### Partially observed markov decision process

`$ M = {S, A, O, T, \epsilon, r} $`

|                                     |                                                     |
| ----------------------------------- | --------------------------------------------------- |
| `$S$` - state space                 | states `$s \in S$` (discrete or continuous)         |
| `$A$` - action space                | actions `$a \in A$` (discrete or continuous)        |
| `$O$` - observation space           | observations `$o \in O$` (discrete or continuous)   |
| `$T$` - Transition operator         | `$ p (s_{t+1} \| s_t ) $`                           |
| `$\epsilon$` - Emission probability | `$ p (o_t \| s_t ) $`                               |
| `$r$` - Reward function             | `$r(s_t, a_t)$` which states and actions are better |

--

![Comparisson](https://github.com/boliu68/boliu68.github.io/blob/master/_posts/one_state_rl.png?raw=true)

--

### Machine Learning

| **Unsupervised** | **Supervised** | **Reinforcement** |
| ---------------- | -------------- | ----------------- |
| Unlabeled data   | Labeled data   | Unlabeled data    |
| Find structure   | Predict        | Predict           |

--

### DQN

![DQN](./res/dqn.png)

--

### A2C

![A2C](./res/a2c.png)

--

### Exploration strategies

![Exploration](./res/exploration.png) <!-- .element: height="50%" width="50%" -->

--

### Gaussian process

![Gaussian](./res/gaussian.png)

<small>Source: arxiv.org/pdf/1602.04450v1.pdf</small>

--

### Unfold MDP

|   |
|---|
|![mdp1](./res/unfold-mdp1.png) |
|![mdp2](./res/unfold-mdp2.png) |

<small>Source: yisongyue.com/publications/aaai2018_safe_mdp.pdf</small>

--

![People](./res/people.png)

--
